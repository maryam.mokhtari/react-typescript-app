import { FC, useEffect, useState } from "react";
import "./App.css";

type UserInformation = {
  firstName: string;
  lastName: string;
  imageSource: string;
  country: string;
};

const App: FC = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [userInformation, setUserInformation] = useState<UserInformation>();
  const [isUserDataShown, setIsUserDataShown] = useState<boolean>(true);
  const [isError, setIsError] = useState<boolean>(false);

  const generateUserInformation = (data: any) => {
    const results = data?.results;
    const userInfo = results?.[0];
    if (userInfo) {
      setUserInformation({
        firstName: userInfo.name?.first,
        lastName: userInfo.name?.last,
        country: userInfo.location?.country,
        imageSource:
          userInfo.picture?.medium ??
          userInfo.picture?.large ??
          userInfo.picture?.thumbnail,
      });
    }
  };

  const onClick = () => {
    if (!isUserDataShown) {
      setIsLoading(true);
    }
    setIsUserDataShown((prevState) => !prevState);
  };

  useEffect(() => {
    const getData = () => {
      setIsError(false);
      fetch("https://randomuser.me/api/")
        .then((response) => response.json())
        .then((data) => {
          generateUserInformation(data);
        })
        .catch(() => {
          setIsError(true);
          setIsUserDataShown(false);
        })
        .finally(() => {
          setIsLoading(false);
        });
    };

    if (isUserDataShown) {
      getData();
    }
  }, [isUserDataShown]);

  return (
    <div className="container">
      {isLoading ? (
        <div data-testid="loading" className="loading">
          Loading...
        </div>
      ) : userInformation ? (
        <>
          <div className="button-wrapper">
            <button data-testid="button" className="button" onClick={onClick}>
              {isUserDataShown ? "Hide" : "Show"}
            </button>
          </div>
          {isError ? (
            <div data-testid="error" className="error">
              Error in fetching data.
            </div>
          ) : (
            isUserDataShown && (
              <div data-testid="userData" className="user-data-container">
                <div data-testid="firstNameRow" className="row">
                  <div data-testid="firstNameTitle" className="title">
                    First name
                  </div>
                  <div data-testid="firstNameValue" className="value">
                    {userInformation?.firstName}
                  </div>
                </div>
                <div data-testid="lastNameRow" className="row">
                  <div data-testid="lastNameTitle" className="title">
                    Last name
                  </div>
                  <div data-testid="lastNameValue" className="value">
                    {userInformation?.lastName}
                  </div>
                </div>
                <div data-testid="imageRow" className="row">
                  <div data-testid="imageTitle" className="title">
                    Image
                  </div>
                  <div data-testid="imageValue" className="value">
                    <img
                      src={userInformation?.imageSource}
                      alt={userInformation?.firstName}
                    />
                  </div>
                </div>
                <div data-testid="countryRow" className="row">
                  <div data-testid="countryTitle" className="title">
                    Country
                  </div>
                  <div data-testid="countryValue" className="value">
                    {userInformation?.country}
                  </div>
                </div>
              </div>
            )
          )}
        </>
      ) : (
        <div data-testid="emptyState" className="empty-state">
          User information is not found.
        </div>
      )}
    </div>
  );
};

export default App;
