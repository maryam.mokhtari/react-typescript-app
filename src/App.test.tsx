import App from "./App";
import { render, fireEvent, waitFor } from "@testing-library/react";
// import fetchMock from "fetch-mock";
// import "@testing-library/jest-dom/extend-expect";

const renderApp = () => render(<App />);

const testLoadingView = () => {
  let { getByTestId, queryByTestId } = renderApp();
  expect(getByTestId("loading")).toHaveTextContent("Loading...");
  expect(queryByTestId("button")).toBe(null);
  expect(queryByTestId("error")).toBe(null);
  expect(queryByTestId("userData")).toBe(null);
  expect(queryByTestId("firstNameRow")).toBe(null);
  expect(queryByTestId("firstNameTitle")).toBe(null);
  expect(queryByTestId("firstNameValue")).toBe(null);
  expect(queryByTestId("lastNameRow")).toBe(null);
  expect(queryByTestId("lastNameTitle")).toBe(null);
  expect(queryByTestId("lastNameValue")).toBe(null);
  expect(queryByTestId("imageRow")).toBe(null);
  expect(queryByTestId("imageTitle")).toBe(null);
  expect(queryByTestId("imageValue")).toBe(null);
  expect(queryByTestId("countryRow")).toBe(null);
  expect(queryByTestId("countryTitle")).toBe(null);
  expect(queryByTestId("countryValue")).toBe(null);
  expect(queryByTestId("emptyState")).toBe(null);
};

test("initial UI is rendered as expected", () => {
  testLoadingView();
});

// test('data is shown by clicking on the show button', async() => {
test("first user's data is shown in first page load", async () => {
  let { getByTestId, queryByTestId } = renderApp();
  await waitFor(
    () => {
      expect(getByTestId("userData")).toBeInTheDocument();
      expect(getByTestId("userData").childNodes).toHaveLength(4);
      expect(queryByTestId("loading")).toBe(null);
      expect(getByTestId("button")).toHaveTextContent("Hide");
      expect(getByTestId("firstNameTitle")).toHaveTextContent("First name");
      expect(getByTestId("lastNameTitle")).toHaveTextContent("Last name");
      expect(getByTestId("imageTitle")).toHaveTextContent("Image");
      expect(getByTestId("countryTitle")).toHaveTextContent("Country");

      expect(getByTestId("firstNameTitle")).toHaveClass("title");
      expect(getByTestId("lastNameTitle")).toHaveClass("title");
      expect(getByTestId("imageTitle")).toHaveClass("title");
      expect(getByTestId("countryTitle")).toHaveClass("title");
      expect(getByTestId("firstNameValue")).toHaveClass("value");
      expect(getByTestId("lastNameValue")).toHaveClass("value");
      expect(getByTestId("imageValue")).toHaveClass("value");
      expect(getByTestId("countryValue")).toHaveClass("value");
      expect(getByTestId("firstNameRow")).toHaveClass("row");
      expect(getByTestId("lastNameRow")).toHaveClass("row");
      expect(getByTestId("imageRow")).toHaveClass("row");
      expect(getByTestId("countryRow")).toHaveClass("row");
      expect(getByTestId("userData")).toHaveClass("user-data-container");
      expect(getByTestId("button")).toHaveClass("button");
    },
    { timeout: 2500 }
  );
});

// test('data is shown by clicking on the show button', async() => {
test("first user's data is shown in first page load", async () => {
  let { getByTestId, queryByTestId } = renderApp();

  await waitFor(
    async () => {
      fireEvent.click(getByTestId("button"));
      await waitFor(
        () => {
          expect(getByTestId("userData")).toBe(null);
          expect(queryByTestId("loading")).toBe(null);
          expect(getByTestId("button")).toHaveTextContent("Show");
        },
        { timeout: 4000 }
      );
    },
    { timeout: 4000 }
  );
});
